/* GTK - The GIMP Toolkit
 * Copyright (C) 1995-1997 Peter Mattis, Spencer Kimball and Josh MacDonald
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include <math.h>
#include "gtkcontainer.h"
#include "gtkscale.h"


#define SCALE_CLASS(w)  GTK_SCALE_CLASS (GTK_OBJECT (w)->klass)


static void gtk_scale_class_init      (GtkScaleClass *klass);
static void gtk_scale_init            (GtkScale      *scale);
static void gtk_scale_destroy         (GtkObject     *object);
static void gtk_scale_draw_background (GtkRange      *range);


static GtkRangeClass *parent_class = NULL;


guint
gtk_scale_get_type ()
{
  static guint scale_type = 0;

  if (!scale_type)
    {
      GtkTypeInfo scale_info =
      {
	"GtkScale",
	sizeof (GtkScale),
	sizeof (GtkScaleClass),
	(GtkClassInitFunc) gtk_scale_class_init,
	(GtkObjectInitFunc) gtk_scale_init,
	(GtkArgFunc) NULL,
      };

      scale_type = gtk_type_unique (gtk_range_get_type (), &scale_info);
    }

  return scale_type;
}

static void
gtk_scale_class_init (GtkScaleClass *class)
{
  GtkObjectClass *object_class;
  GtkRangeClass *range_class;

  object_class = (GtkObjectClass*) class;
  range_class = (GtkRangeClass*) class;

  parent_class = gtk_type_class (gtk_range_get_type ());

  object_class->destroy = gtk_scale_destroy;

  range_class->draw_background = gtk_scale_draw_background;

  class->slider_length = 31;
  class->value_spacing = 2;
  class->draw_value = NULL;
}

static void
gtk_scale_init (GtkScale *scale)
{
  GTK_WIDGET_SET_FLAGS (scale, GTK_CAN_FOCUS);
  GTK_RANGE (scale)->digits = 1;
  scale->draw_value = TRUE;
  scale->value_pos = GTK_POS_TOP;
}

void
gtk_scale_set_digits (GtkScale *scale,
		      gint      digits)
{
  g_return_if_fail (scale != NULL);
  g_return_if_fail (GTK_IS_SCALE (scale));

  if (GTK_RANGE (scale)->digits != digits)
    {
      GTK_RANGE (scale)->digits = digits;

      if (GTK_WIDGET_VISIBLE (scale) && GTK_WIDGET_MAPPED (scale))
	gtk_widget_queue_resize (GTK_WIDGET (scale));
    }
}

void
gtk_scale_set_draw_value (GtkScale *scale,
			  gint      draw_value)
{
  g_return_if_fail (scale != NULL);
  g_return_if_fail (GTK_IS_SCALE (scale));

  if (scale->draw_value != draw_value)
    {
      scale->draw_value = (draw_value != 0);

      if (GTK_WIDGET_VISIBLE (scale) && GTK_WIDGET_MAPPED (scale))
	gtk_widget_queue_resize (GTK_WIDGET (scale));
    }
}

void
gtk_scale_set_value_pos (GtkScale        *scale,
			 GtkPositionType  pos)
{
  g_return_if_fail (scale != NULL);
  g_return_if_fail (GTK_IS_SCALE (scale));

  if (scale->value_pos != pos)
    {
      scale->value_pos = pos;

      if (GTK_WIDGET_VISIBLE (scale) && GTK_WIDGET_MAPPED (scale))
	gtk_widget_queue_resize (GTK_WIDGET (scale));
    }
}

gint
gtk_scale_value_width (GtkScale *scale)
{
  GtkRange *range;
  gchar buffer[128];
  gfloat value;
  gint temp;
  gint return_val;
  gint digits;
  gint i, j;

  g_return_val_if_fail (scale != NULL, 0);
  g_return_val_if_fail (GTK_IS_SCALE (scale), 0);

  return_val = 0;
  if (scale->draw_value)
    {
      range = GTK_RANGE (scale);

      value = ABS (range->adjustment->lower);
      if (value == 0) value = 1;
      digits = log10 (value) + 1;
      if (digits > 13)
	digits = 13;

      i = 0;
      if (range->adjustment->lower < 0)
        buffer[i++] = '-';
      for (j = 0; j < digits; j++)
        buffer[i++] = '0';
      if (GTK_RANGE (scale)->digits)
        buffer[i++] = '.';
      for (j = 0; j < GTK_RANGE (scale)->digits; j++)
        buffer[i++] = '0';
      buffer[i] = '\0';

      return_val = gdk_string_measure (GTK_WIDGET (scale)->style->font, buffer);

      value = ABS (range->adjustment->upper);
      if (value == 0) value = 1;
      digits = log10 (value) + 1;
      if (digits > 13)
        digits = 13;

      i = 0;
      if (range->adjustment->lower < 0)
        buffer[i++] = '-';
      for (j = 0; j < digits; j++)
        buffer[i++] = '0';
      if (GTK_RANGE (scale)->digits)
        buffer[i++] = '.';
      for (j = 0; j < GTK_RANGE (scale)->digits; j++)
        buffer[i++] = '0';
      buffer[i] = '\0';

      temp = gdk_string_measure (GTK_WIDGET (scale)->style->font, buffer);
      return_val = MAX (return_val, temp);
    }

  return return_val;
}

void
gtk_scale_draw_value (GtkScale *scale)
{
  g_return_if_fail (scale != NULL);
  g_return_if_fail (GTK_IS_SCALE (scale));

  if (SCALE_CLASS (scale)->draw_value)
    (* SCALE_CLASS (scale)->draw_value) (scale);
}


static void
gtk_scale_destroy (GtkObject *object)
{
  GtkRange *range;

  g_return_if_fail (object != NULL);
  g_return_if_fail (GTK_IS_SCALE (object));

  range = GTK_RANGE (object);

  if (GTK_OBJECT_CLASS (parent_class)->destroy)
    (* GTK_OBJECT_CLASS (parent_class)->destroy) (object);
}

static void
gtk_scale_draw_background (GtkRange *range)
{
  g_return_if_fail (range != NULL);
  g_return_if_fail (GTK_IS_SCALE (range));

  gtk_scale_draw_value (GTK_SCALE (range));
}
